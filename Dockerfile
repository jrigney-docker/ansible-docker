FROM python:3

RUN pip install ansible

RUN mkdir /etc/ansible/ /ansible
WORKDIR /ansible

ENV ANSIBLE_GATHERING smart
ENV ANSIBLE_HOST_KEY_CHECKING False
ENV ANSIBLE_RETRY_FILES_ENABLED False
ENV ANSIBLE_ROLES_PATH /ansible/roles

ENTRYPOINT ["ansible-playbook"]
