# ansible-docker
Ansible in a Docker container

https://www.ansible.com/



## creating and setting up ansible-docker
1. setup an ansible.cfg
2. build it
3. testing it out
4. using it

### setup a ansible.cfg
```
[defaults]
remote_user = <user the id_rsa is associated with on the remote machine>
```

### build it
```
docker build -t jrigney/ansible .
```

### testing it out
```
docker run --rm -it -v ~/.ssh/id_rsa:/root/.ssh/id_rsa -v ~/.ssh/id_rsa.pub:/root/.ssh/id_rsa.pub -v $(pwd)/production/hosts:/etc/ansible/hosts -v $(pwd):/ansible jrigney/ansible:latest all -m ping
```

### using it
```
docker run --rm -it -v ~/.ssh/id_rsa:/root/.ssh/id_rsa -v ~/.ssh/id_rsa.pub:/root/.ssh/id_rsa.pub -v $(pwd)/production/hosts:/etc/ansible/hosts -v $(pwd):/ansible --entrypoint bash jrigney/ansible:latest
```

## creating ansible project

1. create directories
2. set ansible uid & pwd for ssh connection for playbooks
3. put hosts file in production directory

### creating directories
```
mkdir -p group_vars production
```

## set ansible uid and pwd for ssh connection for playbooks
```
# file: group_vars/all
ansible_connection: ssh 
ansible_ssh_user: vagrant 
ansible_ssh_pass: vagrant
```

## put hosts file in production directory

ssh-copy-id to target machine
docker run --rm -it -v $HOME/.ssh/id_rsa:/root/.ssh/id_rsa -v $(pwd):/ansible jrigney/ansible:1.0 --extra-vars "ansible_user=$USER" -i tmpInv all -m ping

